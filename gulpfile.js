'use strict';

// JSDocs
const jsdoc          = require('gulp-jsdoc3');

// utils
const gulp     	     = require('gulp');
const runSequence    = require('run-sequence');
const plumber        = require('gulp-plumber');
const gutil          = require('gulp-util');

// live-ftp-upload
const connect        = require('gulp-connect');
const ftp            = require('vinyl-ftp');

// html
const pug            = require('gulp-pug');

// css
const sourcemaps     = require('gulp-sourcemaps');
const pleeease       = require('gulp-pleeease');
const rename         = require('gulp-rename');

// js ( see webpack.config file for bundle settings )
const Webpack        = require('webpack');
const webpack_stream = require('webpack-stream');
const webpackConfig  = require('./webpack.config.js');

// live reload with browsersync
const browserSync    = require('browser-sync').create();

// prod/dev variables for different bundles
const NODE_ENV = process.env.NODE_ENV || 'development';
const NODE_DEP = process.env.NODE_DEP || 'livereload';
const development = NODE_ENV == 'development';
const production  = NODE_ENV == 'production';

// configure webpack options
if (development) {
    // set watcher and it's options
    webpackConfig.watch = true;
    webpackConfig.watchOptions = {
        aggregateTimeout: 100
    }
} else if (production) {
    // minify js/css output file
    webpackConfig.plugins.push(
        new Webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                unsafe: true
            }
        })
    );
}
//----------------------------------------FTP TASKS----------------------------------------//

// vars for server connection and deploy task
const user = process.env.FTP_USER;
const password = process.env.FTP_PWD;
const host = '194.28.86.115';
const port = 21;
const localFilesGlob = ['www/**/*'];
const remoteFolder = 'public_html/barbers';

// build an FTP connection
function getFtpConnection() {
    return ftp.create({
        host: host,
        port: port,
        user: user,
        password: password,
        parallel: 5,
        log: gutil.log
    });
}

// upload files to the server on every file change in www/
gulp.task('ftp-deploy-watch', () => {
    let conn = getFtpConnection();
    gulp.watch(localFilesGlob)
        .on('change', function(event) {
            console.log('Changes detected! Uploading file "' + event.path + '", ' + event.type);
            return gulp.src( [event.path], { base: '.', buffer: false } )
                .pipe(plumber())
                .pipe( conn.newer( remoteFolder ) ) // only upload newer files
                .pipe( conn.dest( remoteFolder ) );
        });
});

//-----------------------------------------HTML TASKS---------------------------------------//

function compileHtml(src, dest) {
  return gulp.src([src])
    .pipe(plumber())
    .pipe(pug(pugOptions))
    .pipe(gulp.dest(dest));
}
let pugOptions = {
    pretty: false,
    nspaces: 4,
    tabs: true,
    donotencode: true
};
if ( production )
    pugOptions.pretty = true;

//compile .pug to .html
gulp.task('pug_compile', () => compileHtml('src/templates/pages/*.pug', './www/'));

// watch .pug files
gulp.task('watch-html', () => {
    gulp.watch('src/templates/**/*.pug', ['pug_compile']);
});

//-------------------------------------------CSS TASKS----------------------------------------//

function compileCss(src, dest) {
  gulp.src(src)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(pleeease({
      stylus: {
        paths: ['./src/styl/**']
      },
      browsers: ["last 3 versions", "Android 2.3", "ie 8", "Firefox ESR", "Opera 12.1", "Chrome > 20", "ios 4"],
      minifier: {
        preserveHacks: true,
        removeAllComments: true
      }
    }))
    .pipe(rename({
      suffix: '.min',
      extname: '.css'
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dest));
}

// pleeease
gulp.task('pls', () => compileCss('./src/styl/index.styl', './www/css/'));

// watch for changes
gulp.task('watch-css', () => {
    gulp.watch('src/styl/**/*.styl', ['pls']);
});

//-----------------------------------------WEBPACK TASKS--------------------------------------//

// run webpack
gulp.task('webpack', () => {
    return gulp.src('src/js/index.js')
        .pipe(webpack_stream(webpackConfig))
        .pipe(gulp.dest('www/'));
});

//---------------------------------------LIVE RELOAD TASK-----------------------------------//

// initialize BS and watch changes
gulp.task('livereload', () => {
    browserSync.init({
        server: 'www'
    });
    browserSync.watch('./www/**/*.*').on('change', browserSync.reload);
});

//-----------------------------------------DEFAULT TASK------------------------------------//

// variables for deploy/livereload
const livereload = NODE_DEP == 'livereload';
const deployment = NODE_DEP == 'deployment';

// configure task sequence ( dev/prod )
let tasksArray = ['webpack', 'watch-html', 'watch-css'];

if (livereload) {
  tasksArray.push('livereload');
} else if (deployment) {
  tasksArray.push('ftp-deploy-watch');
}

// default task
gulp.task('default', () => {
    runSequence(tasksArray);
});

//------------------------------------------JSDocs TASK------------------------------------//

gulp.task('JSDocs', function (cb) {
    gulp.src(['README.md', './src/js/**/*.js'], {read: false})
      .pipe(jsdoc(cb));
});