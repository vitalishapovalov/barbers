# Barbers

Barbers web site project.

## Demo

The latest build of this project can be found [here](http://chis.kiev.ua/barbers/www)

## Installing

#### In order to start working with project, you must:

Clone repository to your local machine

```
git clone https://vitalishapovalov@bitbucket.org/vitalishapovalov/barbers.git
```

Install dependencies

```
npm i
```

## NPM scripts

Run production bundle

```
npm run-script runProd
```

Production bundle with deploying to remote server**

```
npm run-script runProdDeploy
```

Run development bundle

```
npm run-script runDev
```

Development bundle with browsersync livereload server

```
npm run-script runDevLivereload
```

Development bundle with deploying to remote server**

```
npm run-script runDevDeploy
```

** In order to start work with ftp, you must edit ftp connection settings in gulpfile.js and then set username and password:

```
set FTP_USER=(username for ftp-connection here)
set FTP_PWD=(password for ftp-connection here)
```

## JSDoc

Install JSDoc globally

```
npm install jsdoc -g
```

Generate docs (once)

```
npm run-script runDocsCompile
```

Run docs
```
npm run-script runDocs
```

## Built with

* [Gulp](http://gulpjs.com/)
* [Pug](https://github.com/pugjs/pug)
* [Webpack](https://webpack.github.io/)
* [Babel](https://babeljs.io/)
* [Pleeease.io](http://pleeease.io/)
* [Styl](https://github.com/tj/styl)
* [Sourcemaps](https://www.npmjs.com/package/gulp-sourcemaps)
* [Browsersync](https://www.browsersync.io/)

## Versioning

Current version is 1.0.0

## Authors

* **Shapovalov Vitali** - *Front-end* - [vitalishapovalov](https://bitbucket.org/vitalishapovalov/)
