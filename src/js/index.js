'use strict';

/**
 * App entry point.
 *
 * @module App
 */

/** Import commonly used libs */
import 'easy-autocomplete';

/** Import common components (initialized by default) */
import Common from 'js/components/Common';
Common.init();

/** Import page controllers */
import Main from 'js/pages/main';
import Catalog from 'js/pages/catalog';
import Authorization from 'js/pages/authorization';

/** Import utility functions */
import {
  currentPage
} from 'js/modules/helpers';

/**
 * Detect current page and run appropriate scripts.
 *
 * @param {String} currentPage - current page detector.
 **/
switch (currentPage) {
  /** Main page */
  case 'main': {
    Main.init();
    break;
  }
  case 'catalog': {
    Catalog.init();
    break;
  }
  case 'authorization': {
    Authorization.init();
    break;
  }
}
