'use strict';

/**
 * Main page scripts.
 *
 * @module Main
 */

/** Import utility functions */
import {
  active,
  $body,
  owlCommonOptions,
  screenWidth,
  initOwlCarousel,
} from '../modules/helpers';

/** Class representing a website's Main page functions. Initialized only on web site's main page. */
export default class Main {
  /**
   * Cache main page's DOM elements for instant access.
   */
  constructor() {
    this.$searchInput = $('.main-search__form input');
    this.searchUrl = 'ajax/main-search.json';

    this.$mastersAndServices = $('.masters-and-services .inner-section__card-container');
    this.$reviews = $('.reviews-container');
    this.$sales = $('.sales-container');
    this.$partners = $('.partners-container');

    this.$justJoinedContainer = $('.just-joined .inner-section__item-container');
    this.$justJoinedControls = $('.just-joined .inner-section__dot');
    this.initAjaxPageChangeUrl = 'ajax/main-ajax-page-change.html';
  }

  /**
   * Initialize easy-autocomplete plug-in on search bar's input.
   *
   * @return {Main}
   */
  autofillInit() {
    const $searchInput = this.$searchInput;
    const url = this.searchUrl;

    const options = {
      getValue: 'text',

      url,

      ajaxSettings: {
        dataType: 'json',
        method: 'POST',
      },

      preparePostData: function (data, text) {
        const city = $body.data('city-id');

        data = {
          phrase: {
            text,
            city,
          },
        };

        return data;
      },

      template: {
        type: 'links',
        fields: {
          link: 'website-link',
        },
      },

      list: {
        showAnimation: {
          type: 'fade',
          time: 150,
        },
        hideAnimation: {
          type: 'fade',
          time: 150,
        },
        maxNumberOfElements: 6,
        match: {
          // @TODO: set 'enabled' to 'false' on production
          enabled: true,
        },
      },
    };

    $searchInput.easyAutocomplete(options);

    return this;
  }

  /**
   * Initialize sliders.
   *
   * @return {Main}
   */
  initSliders() {
    // masters and services carousel
    const mastersAndServicesOptions = {
      ...owlCommonOptions,
      dots: true,
      nav: false,
      items: 1,
      responsive: {
        1024: {
          nav: true,
        },
      },
    };
    initOwlCarousel(this.$mastersAndServices, mastersAndServicesOptions);

    // reviews and sales sections
    const reviewsAndSalesOptions = {
      ...owlCommonOptions,
      responsive: {
        0: {
          items: 1,
          nav: false,
          dots: true,
          margin: 10,
        },
        640: {
          nav: false,
          dots: true,
          items: 2,
        },
        1200: {
          nav: true,
          dots: false,
          items: 3,
        },
      },
    };
    initOwlCarousel(this.$reviews, reviewsAndSalesOptions);
    initOwlCarousel(this.$sales, reviewsAndSalesOptions);

    // partners section
    if (this.$partners.length && screenWidth < 640) {
      const partnersOptions = {
        items: 2,
        stagePadding: 50,
        loop: true,
        nav: false,
        dots: false,
        margin: 10,
      };
      initOwlCarousel(this.$partners, partnersOptions);
    }

    return this;
  }

  /**
   * Initialize 'just joined' section ajax page change.
   *
   * @return {Main}
   */
  initAjaxPageChange() {
    const $justJoinedControls = this.$justJoinedControls;
    const $justJoinedContainer = this.$justJoinedContainer;

    const url = this.initAjaxPageChangeUrl;

    $justJoinedControls.on('click tap', function () {
      const $this = $(this);
      const pageId = $this.data('page-id');

      $.ajax({
        url,
        dataType: 'HTML',
        method: 'GET',
        cache: false,
        data: pageId,
        beforeSend: () => $justJoinedContainer.addClass('loading'),
        complete: () => $justJoinedContainer.removeClass('loading'),
        success: (response) => {
          const $preloader = $justJoinedContainer.find('.new-items-loader').detach();

          $justJoinedContainer.empty().append(response).append($preloader);

          $justJoinedControls.removeClass(active);

          $this.addClass(active);
        },
      });
    });

    return this;
  }

  /**
   * Initialize Main page scripts.
   *
   * @static
   */
  static init() {
    const obj = new this;

    require.ensure([], () => {
      const owl = require('owl.carousel');

      obj.initSliders();
    });

    obj.autofillInit()
      .initAjaxPageChange();
  }
}
