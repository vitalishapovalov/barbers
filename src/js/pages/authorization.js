'use strict';

/**
 * Sign in, Sign up & remind password pages scripts.
 *
 * @module Authorization
 */

/** Form validation tool */
import Validator from '../modules/validator';

/** Class representing a website's Sign in, Sign up & remind password pages page functions. Initialized only on web site's authorization pages. */
export default class Authorization {
  /**
   * Cache catalog page's DOM elements for instant access.
   */
  constructor() {
    // password recovery page
    this.$recoverPasswordForm = $('.authorization-box.password');
    this.recoverPasswordUrl = 'ajax/success.json';
    this.recoverPasswordEventName = 'passwordRecovery';

    // sign in page
    this.$signInForm = $('.authorization-box.signin');
    this.signInUrl = 'ajax/success.json';
    this.signInEventName = 'signIn';

    // sign up page
    this.$signUpForm = $('.authorization-box.signup');
    this.signUpUrl = 'ajax/signup-error.json';
    this.signUpEventName = 'signUp';
  }

  /**
   * 'Recover password' form validation.
   *
   * @return {Authorization}
   */
  initPwdRecovery() {
    const form = this.$recoverPasswordForm;
    const url = this.recoverPasswordUrl;
    const event = this.recoverPasswordEventName;

    const passwordRecovery = new Validator(form, (validator) => {
      const data = {
        event,
        data: validator.serializedFormData(),
      };

      return {
        url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            console.log('password recovery successful');
          } else {
            console.log('password recovery failed');
          }
        },
      };
    });

    return this;
  }

  /**
   * 'Sign in' form validation.
   *
   * @return {Authorization}
   */
  initSignIn() {
    const form = this.$signInForm;
    const url = this.signInUrl;
    const event = this.signInEventName;

    const signIn = new Validator(form, (validator) => {
      const data = {
        event,
        data: validator.serializedFormData(),
      };

      return {
        url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            console.log('sign in successful');
          } else {
            console.log('password recovery failed');
          }
        },
      };
    });

    return this;
  }

  /**
   * 'Sign up' form validation.
   *
   * @return {Authorization}
   */
  initSignUp() {
    const form = this.$signUpForm;
    const url = this.signUpUrl;
    const event = this.signUpEventName;

    const signUp = new Validator(form, (validator) => {
      const data = {
        event,
        data: validator.serializedFormData(),
      };

      return {
        url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            console.log('sign up successful');
          } else if (res.response === 'error') {
            const error = res.error;
            const field = form.find(error.field);
            const text = error.errorText;

            validator.throwError(field, text);
          } else {
            console.log('sign up failed');
          }
        },
      };
    });

    return this;
  }

  /**
   * Initialize Sign in, Sign up & remind password pages scripts.
   *
   * @static
   */
  static init() {
    const obj = new this;

    obj.initPwdRecovery()
      .initSignIn()
      .initSignUp();
  }
}
