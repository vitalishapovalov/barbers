'use strict';

/**
 * Catalog page scripts.
 *
 * @module Catalog
 */

/** Import utility functions */
import {
  $body,
  activeBodyClassItemInit,
  getCheckedLength,
} from '../modules/helpers';

/** Class representing a website's Catalog page functions. Initialized only on web site's catalog page. */
export default class Catalog {
  /**
   * Cache catalog page's DOM elements for instant access.
   */
  constructor() {
    this.$sliders = $('.slider-filter input');

    this.$filtersToggle = $('.catalog-filtersToggle');

    this.$categoryFilters = $('.catalog-filters__categories input');

    this.$selectedTagsContainer = $('.catalog-filters__tags-selected .tag-container');
    this.$tagSearchInput = $('input[name="tagSearch"]');
    this.tagSearchUrl = 'ajax/tag-search.json';
  }

  /**
   * Initialize filters toggle button. (mobile)
   *
   * @return {Catalog}
   */
  initFiltersToggle() {
    activeBodyClassItemInit(this.$filtersToggle, 'filters-opened');

    return this;
  }

  /**
   * Initialize IRS plugin.
   *
   * @return {Catalog}
   */
  initIrs() {
    const $sliders = this.$sliders;

    require.ensure([], () => {
      const ionRangeSlider = require('ion-rangeslider');

      $sliders.each((index, slider) => {
        const $slider = $(slider);

        $slider.ionRangeSlider({
          type: 'double',
          min: $slider.data('min'),
          max: $slider.data('max'),
          from: $slider.data('from'),
          to: $slider.data('to'),
          hide_min_max: true,
          grid: false,
        });
      });
    });

    return this;
  }

  /**
   * Initialize google maps.
   *
   * @return {Catalog}
   */
  initGoogleMaps() {
    const $mapContainer = document.getElementById('map');
    const zoom = 5;
    const center = {
      lat: 48,
      lng: 67.5,
    };
    const styles = [{
      "featureType": "road.highway",
      "stylers": [{"hue": "#FFC200"}, {"saturation": -61.8}, {"lightness": 45.599999999999994}, {"gamma": 1}]
    }, {
      "featureType": "road.arterial",
      "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 51.19999999999999}, {"gamma": 1}]
    }, {
      "featureType": "road.local",
      "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 52}, {"gamma": 1}]
    }, {
      "featureType": "water",
      "stylers": [{"hue": "#0078FF"}, {"saturation": -13.200000000000003}, {"lightness": 2.4000000000000057}, {"gamma": 1}]
    }, {
      "featureType": "poi",
      "stylers": [{"hue": "#00FF6A"}, {"saturation": -1.0989010989011234}, {"lightness": 11.200000000000017}, {"gamma": 1}]
    }];

    window.initMap = () => {

      if (!$mapContainer) return;

      //create map
      const map = new google.maps.Map($mapContainer, {
        center,
        zoom,
        styles,
      });

      /** Google address autocomplete */
      const input = document.getElementById('googleAddress');
      const searchBox = new google.maps.places.SearchBox(input);

      map.addListener('bounds_changed', () => searchBox.setBounds(map.getBounds()));

      let markers = [];

      searchBox.addListener('places_changed', () => {
        const places = searchBox.getPlaces();

        if (places.length == 0) return;

        markers.forEach((marker) => marker.setMap(null));
        markers = [];

        const bounds = new google.maps.LatLngBounds();
        places.forEach((place) => {
          const icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
    };

    $body.append('<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrFjHOCFQbsMvKEGL92MTAffwFyHgogZI&language=ru&signed_in=true&libraries=places&callback=initMap" charset="UTF-8"></script>');

    return this;
  }

  /**
   * Initialize category filters dropdowns.
   *
   * @return {Catalog}
   */
  initCategoriesDropdown() {
    this.$categoryFilters.on('change', function () {
      const $category = $(this).parents('.dropdown');
      const $selected = $category.find('.selected');
      const $checkboxes = $category.find('input');
      const checkboxesLength = $checkboxes.length;
      const checkedCheckboxesLength = getCheckedLength($checkboxes);

      let text;

      if (checkedCheckboxesLength === 0) {
        text = 'Не выбрано';
      } else if (checkedCheckboxesLength === checkboxesLength) {
        text = 'Выбрано все';
      } else {
        text = `Выбрано ${checkedCheckboxesLength} из ${checkboxesLength}`;
      }

      $selected.text(text);
    });

    return this;
  }

  /**
   * Initialize hashtags.
   *
   * @return {Catalog}
   */
  initHashtags() {
    const $selectedTagsContainer = this.$selectedTagsContainer;

    const tagTemplate = text => `
      <div class="tag">
        <span>${text}</span>
        <div class="close">
            <span></span>
            <span></span>
        </div>
      </div>
    `;

    const removeTag = tag => tag.animate({ opacity: 0 }, () => tag.remove());

    // add new tags
    $body.on('click tap', '.catalog-filters__tags .tag', function () {
      const $this = $(this);
      const text = $this.find('span').text();
      const addedTags = $selectedTagsContainer.find('.tag');

      // check for duplicate tags
      for (const tag of addedTags) {
        const $tag = $(tag);
        const addedTagsText = $tag.find('span').text();

        if (text === addedTagsText) return;
      }

      // append tag to added tags section
      $selectedTagsContainer.append(tagTemplate(text));

      // remove tag if in example section
      if ($this.parent().hasClass('example')) removeTag($this);
    });

    // remove added tags
    $body.on('click tap', '.tag .close', function () {
      const $tag = $(this).parent();

      removeTag($tag);

      // тут проверка контейнера на отсутствие тегов, но у меня закончился пивас
    });

    // tag search auto complete
    const $tagSearchInput = this.$tagSearchInput;
    const url = this.tagSearchUrl;

    const options = {
      getValue: 'text',

      url,

      ajaxSettings: {
        dataType: 'json',
        method: 'GET',
      },

      template: {
        type: 'custom',
        method: value => tagTemplate(value),
      },

      list: {
        showAnimation: {
          type: 'fade',
          time: 150,
        },
        hideAnimation: {
          type: 'fade',
          time: 150,
        },
        maxNumberOfElements: 6,
        match: {
          // @TODO: set 'enabled' to 'false' on production
          enabled: true,
        },
      },
    };

    $tagSearchInput.easyAutocomplete(options);

    return this;
  }

  /**
   * Initialize Catalog page scripts.
   *
   * @static
   */
  static init() {
    const obj = new this;

    obj.initFiltersToggle()
      .initIrs()
      .initGoogleMaps()
      .initCategoriesDropdown()
      .initHashtags();
  }
}
