'use strict';

/**
 * Header, Footer, Menu scripts.
 *
 * @module Common
 */

/** Import utility functions */
import {
  $body,
  active,
  toggleDropdown,
  dropdownItemInit,
  Swipedetect,
} from '../modules/helpers';

/** Class representing common functions. Initialized by default. */
export default class Common {

  /**
   * Cache DOM elements.
   */
  constructor() {
    this.$dropdowns = $('.dropdown');
    this.$selectLists = $('.dropdown.city');
    this.$menuTrigger = $('.menu-trigger');
    this.$searchBar = $('.search-bar-small input');
  }

  /**
   * Initialize dropdowns.
   *
   * @return {Common}
   */
  initDropdowns() {
    this.$dropdowns.each((index, dropdown) => {
      const $dropdown = $(dropdown);
      const $trigger = $dropdown.find('.dropdown__title');

      $dropdown.hasClass('list') ? toggleDropdown($trigger, $dropdown) : dropdownItemInit($trigger, $dropdown);
    });

    return this;
  }

  /**
   * Initialize search bar (header & menu).
   *
   * @return {Common}
   */
  initSearch() {
    // add active state on focus
    this.$searchBar
      .focus(function () {
        $(this).parent().addClass(active);
      })
      .blur(function () {
        const $this = $(this);
        if (!$this.val()) $this.parent().removeClass(active);
      });

    return this;
  }

  /**
   * Initialize city select.
   *
   * @return {Common}
   */
  initCitySelect() {
    const $lists = this.$selectLists;
    const $outputTextContainers = $lists.find('.selected');
    const $items = $lists.find('.dropdown__body-inner li');

    $items.on('click tap', function () {
      const $this = $(this);
      const city = $this.text();
      const cityId = $this.data('city-id');

      // show output text
      $outputTextContainers.text(`Город: ${city}`);

      // change active class
      $items.removeClass(active);
      $items.filter(`[data-city-id="${cityId}"]`).addClass(active);

      // set city id data attr to body
      $body.attr('data-city-id', cityId);
    });

    return this;
  }

  /**
   * Initialize menu toggle.
   *
   * @return {Common}
   */
  initMenuToggle() {
    const $menuTrigger = this.$menuTrigger;

    function openMenu() {
      $body.addClass('menu-opened');
      $menuTrigger.trigger('opening');
    }

    function closeMenu() {
      $body.removeClass('menu-opened');
      $menuTrigger.trigger('closing');
    }

    // Open / Close menu on click
    $menuTrigger.on('click tap', () => $body.hasClass('menu-opened') ? closeMenu() : openMenu());

    // Close menu on swipe
    const $swipeableElements = $('header, main, footer');
    const callback  = (direction) => {
      if (direction === 'left') closeMenu();
    };
    const swipeController = new Swipedetect($swipeableElements, callback);
    $menuTrigger
      .on('opening', () => swipeController.bindDetect())
      .on('closing', () => swipeController.unbindDetect());

    // Scroll to top when opening header menu, scroll to bot when opening footer menu
    $menuTrigger.on('click tap', function () {
      const $this = $(this);
      const footerMenuClass = 'menu-footer';
      const headerMenuClass = 'menu-header';
      const $scrolledElements = $('html, body');

      if ($this.hasClass(footerMenuClass)) {
        $scrolledElements.animate(
          { scrollTop: $scrolledElements.height() },
          0,
          () => $body.addClass(footerMenuClass));
      } else {
        if ($this.hasClass(headerMenuClass)) {
          $scrolledElements.animate(
            { scrollTop: 0 },
            0,
            () => $body.removeClass(footerMenuClass));
        }
      }
    });

    return this;
  }

  /**
   * Initialize common components scripts.
   *
   * @static
   */
  static init() {
    const obj = new this;

    obj.initDropdowns()
      .initCitySelect()
      .initMenuToggle()
      .initSearch();
  }
}
