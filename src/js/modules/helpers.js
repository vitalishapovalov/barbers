'use strict';

/**
 * Commonly used constants and functions.
 *
 * @module Helpers
 */

/**
 * CSS class name to indicate most elements state.
 *
 * @constant
 * @type {String}
 */
export const active = 'active';

/**
 * Cache body DOM element.
 *
 * @constant
 * @type {jQuery}
 */
export const $body = $('body');

/**
 * Detect current page.
 *
 * @constant
 * @type {String}
 */
export const currentPage = $body.find('main').data('page');

/**
 * Detect window width once.
 *
 * @constant
 * @type {Number}
 */
export const screenWidth = $(window).width();

/**
 * Detect whether user logged in or not and return true/false flag.
 *
 * @constant
 * @type {Boolean}
 */
export const userLoggedIn = $body.hasClass('logged');

/**
 * Cache has own property method.
 *
 * @constant
 * @type {Function}
 */
export const has = Object.prototype.hasOwnProperty;

/**
 * Custom HTML template for owlCarousel 'Prev' button.
 *
 * @constant
 * @type {String}
 */
export const buttonLeftTemplate = '<div class="arrow"></div>';

/**
 * Custom HTML template for owlCarousel 'Next' button.
 *
 * @constant
 * @type {String}
 */
export const buttonRightTemplate = '<div class="arrow"></div>';

/**
 * Common options for owlCarousel sliders.
 *
 * @type {Object}
 */
export let owlCommonOptions = {
  loop: true,
  dots: false,
  nav: true,
  navText: [buttonLeftTemplate, buttonRightTemplate],
  margin: 10,
};

/**
 * Initialize owl carousel on each element from array.
 *
 * @param {jQuery} elements - array of elements to init owlCarousel on.
 * @param {Object} [options] - options for owlCarousel.
 */
export const initOwlCarousel = (elements, options = owlCommonOptions) => {
  elements.each((index, el) => $(el).owlCarousel(options));
};

/**
 * Detect window width dynamically.
 *
 * @return {Number} - Current window width.
 */
export const screenWidthDynamically = () => $(window).width();

/**
 * Check specified item to be target of event.
 *
 * @param {Object} e - Event object.
 * @param {jQuery} item - Item to compare with.
 * @returns {Boolean} - Indicate whether clicked target is the specified item or no.
 */
export const checkClosest = (e, item) => $(e.target).closest(item).length > 0;

/**
 * Toggles specified class name on body.
 *
 * @param {jQuery} elem - Trigger element.
 * @param {String} className - Class name to apply.
 */
export const activeBodyClassItemInit = (elem, className) => {
  elem.on('click tap', () => $body.toggleClass(className));
};

/**
 * Toggle dropdown on click.
 *
 * @param {jQuery} activeItem - Click event handler.
 * @param {jQuery} toggleItem - Toggling object.
 */
export const toggleDropdown = (activeItem, toggleItem) => {
  activeItem.on('click tap', () => toggleItem.toggleClass(active));
};

/**
 * Remove active class if click outside the element.
 *
 * @param {jQuery} activeItem - Default click event handler to active item.
 * @param {jQuery} toggleItem - Toggling object.
 * @param {String} className
 */
export const hideOnBodyClick = (activeItem, toggleItem, className = active) => {
  $body.on('click tap', (e) => {
    const notToggleItem = !checkClosest(e, toggleItem);
    const notActiveItem = !checkClosest(e, activeItem);
    const toggleItemIsOpened  = toggleItem.hasClass(className);

    if (notActiveItem && notToggleItem && toggleItemIsOpened) toggleItem.removeClass(className);
  });
};

/**
 * Remove specified class name if clicked anywhere outside the specified container.
 *
 * @param {jQuery} element - Container.
 * @param {jQuery} elementToRemoveClass - Remove class from this element.
 * @param {String} className - Class name to remove.
 */
export const removeClassOnBodyClick = (element, elementToRemoveClass = element, className = 'incorrect') => {
  $body.on('click tap', (e) => {
    const clickIsOutsideTheElement = !$(e.target).closest(element).length > 0;

    if (clickIsOutsideTheElement) elementToRemoveClass.removeClass(className);
  });
};

/**
 * Makes default dropdown object with event handlers.
 *
 * @param {jQuery} activeItem - Default click event handler to active item.
 * @param {jQuery} toggleItem - Toggling object.
 */
export const dropdownItemInit = (activeItem, toggleItem) => {
  toggleDropdown(activeItem, toggleItem);
  hideOnBodyClick(activeItem, toggleItem);
};

/**
 * Select item from list: make it active ( while disabling others ), change selected item text.
 *
 * @param {jQuery} item - Clicked item.
 * @param {jQuery} selected - Selected item's text keeper.
 * @param {jQuery|Boolean} list - List of items. 'False' to ignore.
 * @param {String} textPrefix - text inserted before selected item's text
 */
export const selectOneFromListInit = (item, selected, list, textPrefix = '') => {
  item.on('click tap', function () {
    let $this = $(this);

    selected.text(textPrefix + $this.text());

    item.removeClass(active);
    $this.addClass(active);

    typeof list === 'object' ? list.removeClass(active) : null;
  });
};

/**
 * Reduce the set of checkbox elements to checked ones.
 *
 * @param {jQuery} checkboxes - list of items.
 */
export const getChecked = (checkboxes) => $.map(checkboxes, (checkbox) => {
  if ($(checkbox).prop('checked')) return checkbox;
});

/**
 * Get length of checked elements from list.
 *
 * @param {jQuery} checkboxes - list of items.
 */
export const getCheckedLength = (checkboxes) => getChecked(checkboxes).length;

/**
 * Send AJAX with specified callback.
 *
 * @param {String} url
 * @param {Object|Array|String} data
 * @param {Function} callback
 * @param {Object} [context=null] - callback is called within this context
 * @param {String} [method=GET]
 */
export const sendJsonAjaxWithCallback = (url, data, callback, context = null, method = 'GET') => {
  $.ajax({
    url: url,
    method: method,
    dataType: 'json',
    cache: false,
    data: data,
    success: (res) => {
      console.log(`DATA: ${JSON.stringify(data)} \n Response: ${JSON.stringify(res)}`);
      callback.call(context, res);
    },
  });
};

/**
 * Detect swipe direction.
 *
 * @param {HTMLElement|jQuery|String} element
 * @param {Function} callback
 */
export class Swipedetect {

  constructor(element, callback) {
    this.$touchsurface = $(element);
    this.callback = callback;

    this.threshold = 100;
    this.restraint = 100;
    this.allowedTime = 300;
  }

  bindDetect() {
    const $touchsurface = this.$touchsurface;

    $touchsurface.on('touchstart.detecter', (e) => {
      const touchobj = e.changedTouches[0];

      this.swipedir = 'none';

      this.startX = touchobj.pageX;
      this.startY = touchobj.pageY;

      this.startTime = new Date().getTime();
    });

    $touchsurface.on('touchend.detecter', (e) => {
      const touchobj = e.changedTouches[0];

      this.distX = touchobj.pageX - this.startX;
      this.distY = touchobj.pageY - this.startY;

      this.elapsedTime = new Date().getTime() - this.startTime;

      // get direction
      if (this.elapsedTime <= this.allowedTime) {
        if (Math.abs(this.distX) >= this.threshold && Math.abs(this.distY) <= this.restraint) {
          this.swipedir = (this.distX < 0) ? 'left' : 'right';
        } else if (Math.abs(this.distY) >= this.threshold && Math.abs(this.distX) <= this.restraint) {
          this.swipedir = (this.distY < 0) ? 'up' : 'down';
        }
      }

      // callback
      const direction = this.swipedir;
      const callback = this.callback;
      callback.call(null, direction);
    });
  }

  unbindDetect() {
    this.$touchsurface.unbind('touchstart.detecter touchmove.detecter touchend.detecter');
  }
}