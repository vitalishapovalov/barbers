/**
 * @name Validator.js
 * @version 0.1
 * @author Vitali Shapovalov
 * @fileoverview
 * This modules is to validate HTML forms.
 * Text fields, emails, phones, checkobxes etc.
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

;(function ($) {

  /**
   * Validator.js module
   *
   * @constructor
   *
   * @param {HTMLElement|jQuery} form - form to validate
   * @param {Function} ajaxOptions - function that should return AJAX request options
   * @param {Object} [options] - user specified options
   * @param {Boolean} [options.nestedInModal=false] - when true, remove fields incorrect state on modal hide
   * @param {String} [options.fieldsSelector='.form-input'] - form's field selector string
   */
  function Validator(form, ajaxOptions, options) {

    ajaxOptions = ajaxOptions || {};
    options = options || {};

    /**
     * Form to validate.
     * @type {jQuery}
     * @public
     */
    this.form = $(form);

    /**
     * User-specified AJAX options.
     * @type {Function|Object}
     * @public
     */
    this.ajaxOptions = ajaxOptions;

    /**
     * User-specified options.
     * @type {Object}
     * @public
     */
    this.options = {
      modal: options.nestedInModal || false,
      fieldsSelector: options.fieldsSelector || '.form-input',
    };

    /** Initialize */
    this.init();
  }

  /**
   * Default options
   * @public
   */
  Validator.prototype.default = {
    // SELECTORS
    incorrectFields: '.incorrect',
    error: '.error',

    // CLASS NAMES
    incorrectClass: 'incorrect',
    formIsValidClass: 'validated',

    // ERROR MESSAGES TEXT
    fieldEmptyText: 'Заполните поле',
    incorrectPhoneText: 'Введите корректный номер',
    incorrectEmailText: 'Введите корректный Email',

    // PARAM NAME FOR SETTING CUSTOM ERROR MESSAGES
    textDataName: 'validation-text',

    // 'data-validation-*' TYPES
    dataCondition: 'validation-condition',
    dataType: 'validation-type',

    // FIELD VALIDATION TYPES
    textType: 'text',
    phoneType: 'phone',
    emailType: 'email',
    checkboxType: 'checkbox',
    radioType: 'radio',

    // 'data-validation-condition' VALUE TYPES
    minLengthDataName: 'min-length',
    lengthDataName: 'length',
    equalsDataName: 'equals',

    // 'data-validation' VALUE TYPES
    requiredToValidate: 'required',
  };

  /**
   * DOM elements
   * @public
   */
  Validator.prototype.elements = function () {
    var _this = this;

    // DYNAMIC ELEMENTS SELECTORS
    return {
      inputs: function () {
        return _this.form.find('input, textarea, select');
      },
      fields: function () {
        return _this.form.find(_this.options.fieldsSelector);
      },
    };
  };

  /**
   * 'Modal' DOM element
   * @public
   * @return {jQuery} modal
   */
  Validator.prototype.modal = function () {
    return this.form.parents('.modal');
  };

  /**
   * 'Validation button' DOM element
   * @public
   * @return {jQuery} button
   */
  Validator.prototype.button = function () {
    return this.form.find('.validate-form-button');
  };

  /**
   * Buffer object
   * @public
   */
  Validator.prototype.buffer = {};

  /**
   * Check form for validness.
   *
   * @return {Boolean}
   */
  Validator.prototype.formIsValid = function () {
    var incorrectFieldsLength = this.form.find(this.default.incorrectFields).length;
    var formIsValidated = this.form.hasClass(this.default.formIsValidClass);

    return !incorrectFieldsLength && formIsValidated;
  };

  /**
   * Validates an email.
   *
   * @static
   *
   * @param {String} email
   * @return {Boolean}
   */
  Validator.validateEmail = function (email) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

    return pattern.test(email);
  };

  /**
   * Remove incorrect state from all fields.
   *
   * @return {Validator}
   */
  Validator.prototype.removeIncorrectState = function () {
    this.form
      .find(this.options.fieldsSelector).removeClass(this.default.incorrectClass);

    return this;
  };

  /**
   * Reset form to default state.
   *
   * @return {Validator}
   */
  Validator.prototype.resetForm = function () {
    this.form[0].reset();

    return this;
  };

  /**
   * Remove incorrect state from all fields when modal is closed.
   *
   * @return {Validator}
   */
  Validator.prototype.removeIncorrectStateOnModalClose = function () {
    var _this = this;

    _this.elements.modal.on('hidden.bs.modal', function () {
      _this.removeIncorrectState();
    });

    return this;
  };

  /**
   * Set incorrect state on field.
   *
   * @param {jQuery} field
   * @param {String} errorText - displayed error text
   */
  Validator.prototype.throwError = function (field, errorText) {
    field.addClass(this.default.incorrectClass)
      .find(this.default.error).text(errorText);
  };

  /**
   * Check field for validness and set valid/incorrect state.
   *
   * @param {jQuery} field
   * @param {Number} valueLength
   * @param {String} errorText
   * @param {Boolean} condition - condition to set valid state
   */
  Validator.prototype.checkFieldValidness = function (field, condition, errorText, valueLength) {
    var dataText = field.data(this.default.textDataName);

    if (dataText && dataText.length) errorText = dataText;

    if (!valueLength) {
      this.throwError(field, this.default.fieldEmptyText);
    } else if (!condition) {
      this.throwError(field, errorText);
    } else {
      this.form.addClass(this.default.formIsValidClass);
    }
  };

  /**
   * Validates field.
   *
   * @param {jQuery} field
   */
  Validator.prototype.validateField = function (field) {
    var type = field.data(this.default.dataType);
    var fieldParams = {
      condition: true,
      errorText: this.default.fieldEmptyText,
      length: 1,
    };

    switch (type) {
      case this.default.textType: {
        fieldParams = this.validateTextField(field);
        break;
      }
      case this.default.phoneType: {
        fieldParams = this.validateTextField(field);
        fieldParams.errorText = this.default.incorrectPhoneText;
        break;
      }
      case this.default.emailType: {
        fieldParams = this.validateEmailField(field);
        fieldParams.errorText = this.default.incorrectEmailText;
        break;
      }
      case this.default.radioType: {
        fieldParams.condition = this.validateRadioField(field);
        break;
      }
    }

    this.checkFieldValidness(field, fieldParams.condition, fieldParams.errorText, fieldParams.length);
  };

  /**
   * Validate 'Text' field.
   *
   * @param {jQuery} field
   * @return {Object}
   */
  Validator.prototype.validateTextField = function (field) {
    var input = field.find('input').length ? field.find('input') : field.find('textarea');
    var value = input.val();
    var valueLength = value.length;

    var conditionType = input.data(this.default.dataCondition);
    var condition, errorText;

    switch (conditionType) {
      case this.default.minLengthDataName: {
        var minLength = input.data(this.default.minLengthDataName);

        condition = valueLength >= parseInt(minLength, 10);
        errorText = 'Минимальная длинна поля - ' + minLength + ' символа';

        break;
      }
      case this.default.lengthDataName: {
        var neededLength = input.data(this.default.lengthDataName);

        condition = valueLength === parseInt(neededLength, 10);
        errorText = 'Необходимая длинна поля - ' + neededLength + ' символов';

        break;
      }
      case this.default.equalsDataName: {
        var neededEqual = input.data(this.default.equalsDataName);

        condition = value === neededEqual;
        errorText = 'Значение поля не совпало с ожидаемым';

        break;
      }
    }

    return {
      condition: condition,
      errorText: errorText,
      length: valueLength,
    };
  };

  /**
   * Validate 'Email' field.
   *
   * @param {jQuery} field
   */
  Validator.prototype.validateEmailField = function (field) {
    var value = field.find('input').val();

    return {
      condition: Validator.validateEmail(value),
      length: value.length,
    };
  };

  /**
   * Validate 'Radio' field.
   *
   * @param {jQuery} field
   */
  Validator.prototype.validateRadioField = function (field) {
    var checkedVisibleRadio = field.find('input[type="radio"]:checked:visible');

    return checkedVisibleRadio.length >= 1;
  };

  /**
   * Serialize form.
   *
   * @return {Array} serialized form data
   */
  Validator.prototype.serializedFormData = function () {
    return this.form.serialize();
  };

  /**
   * Send data if form is valid.
   *
   * @param {Object|Function} options - ajax options
   * @return {Validator}
   */
  Validator.prototype.sendIfValidated = function (options) {
    const _this = this;

    options = options || _this.ajaxOptions;

    function callAjaxWithOptions() {
      switch (typeof options) {
        case 'function': {
          $.ajax(options(_this));
          break;
        }
        case 'object': {
          $.ajax(options);
          break;
        }
        default: {
          throw new Error('ajaxOptions must be a function or an object');
        }
      }
    }

    if (this.formIsValid()) callAjaxWithOptions();

    return this;
  };

  /**
   * Validate form fields.
   *
   * @return {Validator}
   */
  Validator.prototype.validateAllFields = function () {
    var _this = this;

    _this.elements().fields().each(function (index, field) {
      var $field = $(field);
      var requiredToValidate = $field.data('validation') === _this.default.requiredToValidate;

      if (requiredToValidate) _this.validateField($field);
    });

    return this;
  };

  /**
   * Validate form.
   *
   * @return {Validator}
   */
  Validator.prototype.runFormValidation = function () {
    this.removeIncorrectState()
      .validateAllFields()
      .sendIfValidated();

    return this;
  };

  /**
   * Initialize on-click validation.
   *
   * @return {Validator}
   */
  Validator.prototype.bindOnClickValidation = function () {
    var _this = this;

    _this.button().on('click.validation tap.validation', function (e) {
      e.preventDefault();

      _this.runFormValidation();
    });

    return this;
  };

  /**
   * Unbind on-click event.
   *
   * @return {Validator}
   */
  Validator.prototype.unbindOnClick = function () {
    this.elements.button.unbind('click.validation tap.validation');

    return this;
  };

  /**
   * Initialize all validation scripts.
   */
  Validator.prototype.init = function () {
    this.bindOnClickValidation();

    if (this.options.modal) this.removeIncorrectStateOnModalClose();
  };

  /* expose Validator */
  window.Validator = Validator;

  /* expose Validator */
  if (typeof module === 'object' && typeof module.exports === 'object') {
    // CommonJS, just export
    module.exports = Validator;
  } else if (typeof define === 'function' && define.amd) {
    // AMD support
    define('Validator', function () { return Validator; });
  }

})(jQuery);
