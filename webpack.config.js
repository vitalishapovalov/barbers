'use strict';

const webpack  			= require('webpack');
const path 	   			= require("path");
const WebpackAssetsManifest = require('webpack-assets-manifest');
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');

module.exports = {

	entry: {
		"js/index": "./src/js/index"
	},

	output: {
		path: path.resolve(__dirname, 'www'),
		publicPath: './',
		filename: "[name].bundle.js",
    chunkFilename: "js/chunks/[chunkhash].js"
	},

	devtool: "source-map", // Generate sourcemap files.

	plugins: [
		new WebpackMd5Hash(),

		new webpack.ProvidePlugin({	  // Creates global variables. Better use with common libraries (like jQuery, underscore, lodash)
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		}),
		new webpack.NoErrorsPlugin(), // If any errors occurred during webpack process - don't generate output.

		new webpack.ContextReplacementPlugin( /\.\/locale/, /ru|en-gb/ ), // If required path matches (in this case - /\.\/locale/) then
					 													 // require only matched files (in this case - /ru|en-gb/).

		/*new webpack.optimize.CommonsChunkPlugin({	// Generates separate file with code, used by all entry-point-js-files, 
			name: 'common'							// but cuts this code from them. Has many options (see documentation).
		})	*/										// Needs to be included inline.
			// If global variable is set to specified value -		// push additional plugin (in this case - minify js).
		new WebpackAssetsManifest({
			output: 'webpack-assets-manifest.json',
			replacer: null,
			space: 2,
			writeToDisk: false,
			sortManifest: true,
			merge: false,
			publicPath: '',
			done: (manifest) => {
				console.log(`The manifest has been written to ${manifest.getOutputPath()}`);
			}
		}),
		new ChunkManifestPlugin({
			filename: "webpack-chunk-manifest.json",
			manifestVariable: "webpackManifest"
		})
	],

	resolve: {
		modulesDirectories: ['node_modules'], // look for modules only in specified directory,
		extensions: ['', '.js'],	  // with specified extensions.
    root: [path.join(__dirname, './src')], // Adds this prefix to all imported paths.
		alias: {}
	},

	module: {
	  loaders: [							  // Handle files loading with specified plugins (babel etc.).
	    {
	      loader: 'babel',
	      test: /\.js$/,
	      exclude: [/node_modules/, path.resolve(__dirname, 'src/js/modules/dep')],
	      query: {
		      presets: ['es2015', 'stage-2']
	      }
	    }
	  ]
	}
};